create database DB_HR

create table tb_karyawan
(
	id bigint primary key identity(1,1),
	nip varchar (50) not null,
	nama_depan varchar (50) not null,
	nama_belakang varchar (50) not null,
	jenis_kelamin varchar (50) not null,
	agama varchar (50) not null,
	tampat_lahir varchar (50) not null,
	tgl_lahir date null,
	alamat varchar (100) not null,
	pendidikan_terakhir varchar (50) not null,
	tgl_masuk date null,
)

--alter table tb_karyawan alter column tampat_lahir 'tempat_lahir'
insert into tb_karyawan(nip, nama_depan, nama_belakang, jenis_kelamin, agama, tampat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
values
	('001' , 'Hamidi' , 'Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
	('002' , 'Ghandi' , 'Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01'),
	('003' , 'Paul' , 'Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12')

create table tb_divisi
(
	id bigint primary key identity(1,1),
	kd_divisi varchar (50) not null,
	nama_divisi varchar (50) not null
)
insert into tb_divisi(kd_divisi, nama_divisi)
values
	('GD' , 'Gudang'),
	('HRD' , 'HRD'),
	('KU' , 'Keuangan'),
	('UM' , 'Umum')

create table tb_jabatan
(
	id bigint primary key identity(1,1),
	kd_jabatan varchar (50) not null,
	nama_jabatan varchar (50) not null,
	gaji_pokok numeric null,
	tunjangan_jabatan numeric null
)
insert into tb_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
values
	('MGR', 'Manager', '5500000', '1500000'),
	('OB', 'Office Boy', '1900000', '200000'),
	('ST', 'Staff', '3000000', '750000'),
	('WMGR', 'Wakil Manager', '4000000', '1200000')

create table tb_pekerjaan
(
	id bigint primary key identity(1,1),
	nip varchar (50) not null,
	kode_jabatan varchar (50) not null,
	kode_divisi varchar(50) not null,
	tunjangan_kinerja numeric null,
	kota_penempatan varchar (50) null
)
insert into tb_pekerjaan(nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
values
	('001', 'ST', 'KU', '750000', 'Cianjur'),
	('002', 'OB', 'UM', '350000', 'Sukabumi'),
	('003', 'MGR', 'HRD', '1500000', 'Sukabumi')

-- Nomor 1  Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta
select CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap, J.nama_jabatan,
J.tunjangan_jabatan + J.gaji_pokok as gaji_tunjangan
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
group by J.gaji_pokok, J.tunjangan_jabatan, K.nama_belakang, K.nama_depan,
J.nama_jabatan
having J.tunjangan_jabatan + J.gaji_pokok < 5000000
order by nama_depan desc

-- Nomor 2 Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi
select CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap,
J.nama_jabatan, D.nama_divisi, 
J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja as total_gaji,
(J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja) * 0.05 as pajak,
(J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja) - 
((J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja) * 0.05)
as gaji_bersih
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
join tb_divisi D
on P.kode_divisi = D.kd_divisi
where K.jenis_kelamin = 'Pria' and not P.kota_penempatan = 'Sukabumi'

-- Nomor 3 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
select K.nip, CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap,
J.nama_jabatan, D.nama_divisi, 
(0.25 * (J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja)) * 7
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
join tb_divisi D
on P.kode_divisi = D.kd_divisi
order by SUBSTRING(J.nama_jabatan, DATALENGTH(J.nama_jabatan), 1) asc

-- Nomor 4 Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
select P.nip, CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap,
J.nama_jabatan, D.nama_divisi,
J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja as total_gaji,
(J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja) * 0.05 as infak
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
join tb_divisi D
on P.kode_divisi = D.kd_divisi
where J.kd_jabatan = 'MGR'

-- Nomor 5 Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
select P.nip, CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap,
J.nama_jabatan, K.pendidikan_terakhir, 2000000 as tunjangan_pendidikan,
J.gaji_pokok + J.tunjangan_jabatan + 2000000 as total_gaji
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
where K.pendidikan_terakhir like '%S1%'
order by nip 

-- Nomor 6 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
select P.nip, CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap,
J.nama_jabatan, D.nama_divisi,
case 
	when J.kd_jabatan = 'MGR' then 
	(0.25 * (J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja)) * 7
	when J.kd_jabatan = 'ST' then 
	(0.25 * (J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja)) * 5
	else
	(0.25 * (J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja)) * 2
end 
as bonus
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
join tb_divisi D
on P.kode_divisi = D.kd_divisi
order by SUBSTRING(J.nama_jabatan, DATALENGTH(J.nama_jabatan), 1) asc


-- Nomor 7 Buatlah kolom nip pada table karyawan sebagai kolom unique
alter table tb_karyawan add constraint unique_nip unique (nip)

alter table tb_karyawan drop constraint unique_nip

-- Nomor 8 Buatlah kolom nip pada table karyawan sebagai index
create index index_nip on tb_karyawan (nip)


-- Nomor 9 Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
select CONCAT(k.nama_depan,' W', SUBSTRING(k.nama_belakang, 1, (DATALENGTH(K.nama_belakang)-1)),
Upper( SUBSTRING(k.nama_belakang, (DATALENGTH(K.nama_belakang)), 1) ) )as nama_lengkap
from tb_karyawan K

select * from tb_karyawan

-- Nomor 10 Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun
-- Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja
select CONCAT(k.nama_depan,' ', k.nama_belakang) as nama_lengkap, K.tgl_masuk,
J.nama_jabatan, D.nama_divisi, 
J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja as total_gaji,
(J.gaji_pokok + J.tunjangan_jabatan + P.tunjangan_kinerja) * 0.1 as bonus,
floor(datediff(DAY ,K.tgl_masuk, GETDATE())/365.25) as lama_bekerja
from tb_karyawan K join tb_pekerjaan P 
on K.nip = P.nip
join tb_jabatan J
on P.kode_jabatan = J.kd_jabatan
join tb_divisi D
on P.kode_divisi = D.kd_divisi

where  floor(datediff(DAY ,K.tgl_masuk, GETDATE())/365.25) >=  '8'