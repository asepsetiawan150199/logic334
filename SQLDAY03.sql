-- SQLDAY03

-- create stored procedure
create procedure sp_RetieveMahasiswa
--paramaternya disini
as 
begin
	select name, address, email, nilai from mahasiswa
end

-- run / exec procedure
exec sp_RetieveMahasiswa

-- edit / alter stored procedured
alter procedure sp_RetieveMahasiswa
	@id int,
	@name varchar (50)
as 
begin
	select name, address, email, nilai from mahasiswa
	where id = @id and name = @name
end

-- run dengan parameter
exec sp_RetieveMahasiswa 2, 'imam'

-- FUNCTION
-- create function
create function fn_total_mahasiswa
(
	@id int
)
returns int
begin
	declare @hasil int
	select @hasil = count(id) from mahasiswa where id = @id
	return @hasil
end

-- run function
select dbo.fn_total_mahasiswa(1)

-- edit / alter function
alter function fn_total_mahasiswa
(
	@id int, 
	@nama varchar(50)
)
returns int
begin
	declare @hasil int
	select @hasil = count(id) from mahasiswa where id = @id and name = @nama
	return @hasil
end

-- run function

select dbo.fn_total_mahasiswa(2, 'imam')

-- coba select function mahasiswa
select mhs.id, mh.name
from mahasiswa mhs
join biodata bio on bio.mahasiswa_id = mhs.id

-- menghapus semua data dan reset id
truncate table [nama tabel]