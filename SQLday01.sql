--SQLday01

-----------------------------------------------
-- Ini adalah DDL (Data Definition Language) --
-----------------------------------------------

-- Create Database / Membuat Database
create database db_kampus

-- Beralih / Pindah database
use db_kampus

-- Drop Table / Menghapus Database
drop database db_kampus

-- Create Table / Membuat Table
create table mahasiswa
(
	id bigint primary key identity (1,1),
	name varchar (50) not null,
	address varchar (50) not null,
	email varchar (255) null
)

-- Create View / Membuat View
create view vw_mahasiswa
as select * from mahasiswa

-- Menampilkan view
select * from vw_mahasiswa

-----------
-- Alter --
-----------

-- Add Column / Menambah Kolom
alter table mahasiswa add nomor_hp varchar (100) not null

-- Drop Column / Menghapus Kolom
alter table mahasiswa drop column nomor_hp

-- Alter Column / Mengubah Kolom
alter table mahasiswa alter column email varchar (100) not null

----------
-- Drop --
----------

-- Drop Database / Menghapus Database *(Usahakan Menghapus Database Diluar Database Yang Sedang DIkerjakan
drop database [nama database]

-- Drop Table / Menghapus Table
drop table [nama table]

-- Drop view / Menghapus View
drop view [nama view]

-------------------------------------------------
-- Ini Adalah DML (Data Manipulation Language) --
-------------------------------------------------

-- Insert Column / Menambah Isi Kolom
insert into mahasiswa (name, address, email)
values
	('Haikal' , 'Kuningan' , 'haikal5nsah@gmail.com'),
	('Imam' , 'Bekasi' , 'imamassidqi@gmail.com'),
	('Irvan' , 'Jakarta' , 'aliirvan122@gmail.com'),
	('Rezky' , 'Cengkareng' , 'rezkyfajri108@gmail.com'),
	('Tunggul' , 'Semarang' , 'tunggulyudhaputra5@gmail.com'),
	('Shabrina' , 'Palembang' , 'shabrinaputri1601@gmail.com'),
	('tes' , 'kuningan' , 'haikal5nsah@gmail.com')

-- Select Isi Column / Memanggil isi Coloum Dari Database
select id , name , address , email from mahasiswa

-- Select Seluruh Column / Memanggil isi Coloum Dari Database
select top 5 * from mahasiswa order by address desc

-- Update Isi Column / Memperbarui Isi Coloum Dari Database
update mahasiswa set name = 'Haikal Limansyah' where id = 1

-- Delete Isi Column / Menghapus Isi Kolom Dari Database
delete mahasiswa where id = 7

----------
-- Join --
----------

-- Create Table Biodata
create table biodata
(
	id bigint primary key identity (1,1),
	mahasiswa_id bigint null,
	tgl_lahir date null,
	gender varchar (10) null
)

insert into biodata (mahasiswa_id, tgl_lahir, gender)
values
	(1, '2020-06-10' , 'Pria'),
	(2, '2021-07-10' , 'Pria'),
	(3, '2022-08-20' , 'Wanita')

update biodata set mahasiswa_id = '3' where id = 3

select * from biodata

-- Join (And , Or , Not)
--bisa tanpa as
select mhs.id as id_mahasiswa, mhs.name as nama_mahasiswa, mhs.address as alamat, mhs.email,
	   bio.tgl_lahir as tanggal_lahir, bio.gender kelamin
from mahasiswa mhs join biodata as bio
on mhs.id = bio.id
where mhs.id = 2 and mhs.name = 'imam' or not mhs.name = 'irvan' and not mhs.id = 3

-- Order by (asc atau desc) jika tanggal_lahir desc ada kembar maka si gender akan berubah jadi asc
select * from biodata order by tgl_lahir desc, gender asc

-- top
select top 1 * from biodata order by mahasiswa_id desc

-- between
select * from biodata where mahasiswa_id between 2 and 3
select * from biodata where mahasiswa_id >= 2 and mahasiswa_id <= 3
select * from biodata where tgl_lahir between '2019-01-01' and '2021-01-01'

-- like
select * from mahasiswa
where 
	--name like 'a%' -- awalan a kebelakangnya bebas
	--name like '%a' -- akhirannya a awalannya bebas
	--name like '%ez%' -- mengandung ez (bebas awal/akhir)
	--name like '_a%' -- karakter keduanya a dan lenght minimal 2
	--name like 'a__%' -- karaker pertamanya a dan lenght minimal 3
	name like 'a%o' -- awalan a dan akhiran o 

-- Group By
select name from mahasiswa group by name

-- Aggregate function(Sum, count, avg, min, max)
select sum(id), name, address from mahasiswa group by name, address

-- jika update penting untuk menetukan wherenya terlebih dahulu
update mahasiswa set address = 'Jogja' where id = 9

-- having
select top 1 sum (id) as jumlah, name
from mahasiswa
--where name = 'tunggul'
group by name
having sum(id) >= 13
--order by jumlah descs
