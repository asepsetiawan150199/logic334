﻿//PerulnganWhile();
//PerulanganWhile2();
//PerulanganDoWhile();
//PerulanganFor();
//PerulanganBreak();
//PerulanganContinue();
//PerulanganForNested();
//ForEach();
RemoveString();
//InsertString();
//SplitAndJoinString();
//ContainString();
//ToCharArray();

static void ConvertAll()
{
    Console.WriteLine("Masukan Inpu anga (dengan koma) : ");
    string[] input = Console.ReadLine().Split(",");

    int sum = 0;

    int[] array  = Array.ConvertAll(input, int.Parse);

    foreach(int i in array)
    {
        sum += i;
    }
    Console.WriteLine($"jumlah = {sum}");
}
static void ToCharArray(){
    Console.WriteLine("== String To Char Array");
    Console.Write("Masukan Kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();
    
    foreach (char c in array)
    {
        Console.Write(c+ "\t");
    }
    for (int i = 0; i < array.Length; i++)
    {
        Console.Write(array[i]+ "\t");
    }
}
static void Lenght()
{
    string kata = Console.ReadLine();
    Console.WriteLine($"kata {kata.ToUpper()} panjang karakternya = {kata.Length}");
}
static void RemoveString()
{
    Console.Write("Masukan Kata: ");
    string kata = Console.ReadLine() ;
    Console.Write("Masukan Index : ");
    int index = int.Parse(Console.ReadLine());
    Console.WriteLine($"{kata.Remove(index)}");
}static void InsertString()
{
    Console.Write("Masukan Kata: ");
    string kata = Console.ReadLine() ;
    Console.Write("Masukan Index : ");
    int index = int.Parse(Console.ReadLine());
    Console.WriteLine($"{kata.Remove(index)}");
    Console.Write("Masukan Kata Yang Akan Diubah : ");
    string insert = Console.ReadLine();
    Console.WriteLine($"{kata.Insert(index,insert)}");
}
static void ReplaceString()
{
    Console.Write("Masukan Kata: ");
    string kata = Console.ReadLine();
    Console.Write("Masukan Kata Yang Ingin Diubah : ");
    string inser = Console.ReadLine();
    Console.Write("Masukan Kata Yang Akan Diubah : ");
    string replace = Console.ReadLine();
    Console.WriteLine($"{kata.Replace(inser, replace)}");
}
static void SplitAndJoinString()
{
    Console.Write("Masukan Kalimat: ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukan Pemisah : ");
    string split = Console.ReadLine();

    string[] katakata = kalimat.Split(split);
    foreach (string kata in katakata)
    {
        Console.Write(kata);
    }
    Console.WriteLine(string.Join("_", katakata));
    Console.WriteLine();
    int[] deret = {1,2,3,4,5};
    Console.WriteLine(string.Join(" ", deret));
}
static void SubString()
{
    Console.Write("Masukan Kode : ");
    string kata = Console.ReadLine() ;
    Console.Write("Masukan Parameter 1 : ");
    int p1= int.Parse(Console.ReadLine());
    Console.Write("Masukan Parameter 2 : ");
    int p2= int.Parse(Console.ReadLine());
    if (p2 == 0)
    {
        Console.WriteLine($"Hasil SubString = {kata.Substring(p1)}");
    }
    else
    {
        Console.WriteLine($"Hasil SubString = {kata.Substring(p1, p2)}");
    }
}
static void ContainString()//Sering dipake untuk searching
{
    Console.Write("Masukan Kode : ");
    string kata = Console.ReadLine();
    Console.Write("Masukan Parameter 1 : ");
    string contain = Console.ReadLine();

    if (kata.Contains(contain))
    {
        Console.WriteLine($"Kata {kata} mengandung {contain}");
    }
    else
    {
        Console.WriteLine($"Kata {kata} tidak mengandung {contain}");
    }
}
static void PerulnganWhile()
{
    Console.WriteLine("== Perulangan While ==");
    Console.Write("Masukan Perukangan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while(nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++; //increment
    }
}



static void PerulanganWhile2()
{
    Console.WriteLine("== Perulangan While 2 ==");
    Console.Write("Masukan Perukangan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    bool ulangi = true;

    while (ulangi) //ualngi == true jika !ulangi == false
    {
        Console.WriteLine($"Proses ke {nilai}");
        nilai++; //increment

        Console.WriteLine("Ulangi Proses ? (y/n) : ");
        string input = Console.ReadLine();//ToLower bisa ditao di sebelah readline

        if(input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}

static void PerulanganDoWhile()
{
    Console.WriteLine("== Perulangan Do While ==");
    Console.WriteLine("Masukan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 6);
}

static void PerulanganFor()
{
    Console.WriteLine("== Perulangan For ==");
    Console.Write("Masukan Nilai = ");
    int nilai = int.Parse(Console.ReadLine());

    for(int i = 0; i < nilai; i++)
    {
        Console.Write(i+ "\t");
    }
    Console.Write("\n"); //\n artinya enter \t artinya tab
    for(int i = nilai; i >= 0; i--)
    {
        Console.WriteLine(i);
    }
}

static void PerulanganBreak()
{
    for(int i = 0; i<10; i++)
    {
        if (i == 6)
        {
            break;
        }
        Console.WriteLine(i);
    }
}static void PerulanganContinue()
{
    for(int i = 0; i<10; i++)
    {
        if (i == 6)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}
static void PerulanganForNested()
{
    Console.WriteLine("== Perulangan For Nested");
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            Console.Write($"({i},{j})");
        }
        Console.WriteLine();
    }
}
static void ForEach()
{
    int[] array = { 89, 90, 91, 92, 93 };
    int sum = 0;

    foreach(int i in array)
    {
        sum += i;
    }
    Console.WriteLine(sum);
    sum = 0;
    for(int i = 0; i < array.Length; i++)
    {
        sum += array[i];
    }
    Console.WriteLine(sum);
}