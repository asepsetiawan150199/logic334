﻿using System.Diagnostics;

class program
{
    static void Main(string[] args)
    {
        Pembuka();   
    }
    static void Pembuka()
    {/*
        Console.WriteLine("Lingkaran \nPersegi \nPersegi Panjang \nSegitiga");*/
        Console.Write("Pilih nomor soal 1 sampai 8 : ");
        string rumus = Console.ReadLine().ToLower();
        switch (rumus)
        {
            case "1":
                Nomor1();
                break;
            case "2":
                Nomor2();
                break;
            case "3":
                Nomor3();
                break;
            case "4":
                Nomor4();
                break;
            case "5":
                Nomor5();
                break;
            case "6":
                Nomor6();
                break;
            case "7":
                Nomor7();
                break;
            case "8":
                Nomor8();
                break;
            default:
                Console.WriteLine("Anda Salah Input");
                Pembuka();
                break;
        }
    }
    static void Penutup()
    {
        Console.WriteLine("===========================================");
        Console.Write("Apakah masih ingin melanjutkan ? Ya/Tidak : ");
        string comfirm = Console.ReadLine().ToLower();
        switch(comfirm)
        {
            case "ya":
                Pembuka();
                break;
            case "tidak":
                ;
                break;
            default:
                Console.WriteLine("Anda Salah Input");
                break;
            }
        }
    static void Lingkaran()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Mencari Luas dan Keliling Lingkaran ==");
        Console.Write("Masukan Jari-jari Lingkaran : ");
        int r = int.Parse(Console.ReadLine());
        //double pi = 3.14;
        double l = Math.PI * r * r;
        double k = 2 * Math.PI * r;
        Console.WriteLine($"Luas Lingkarannnya adalah :{Math.Round(l, 2)}");
        Console.WriteLine($"Keliling Lingkarannnya adalah :{k}");
        Console.WriteLine("==========================================");
        Console.WriteLine();
        Penutup();
    }

    static void Persegi()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Mencari Luas dan Keliling Persegi ==");
        Console.Write("Masukan Sisi Persegi : ");
        int s = int.Parse(Console.ReadLine());
        int l = s * s;
        int k = 4 * s;
        Console.WriteLine($"Luas Perseginya adalah :{l}");
        Console.WriteLine($"Keliling Perseginya adalah :{k}");
        Console.WriteLine("==========================================");
        Console.WriteLine();
        Penutup();
    }
    static void Nomor1()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Grade Nilai ==");
        Console.Write("Masukan Nilai : ");
        int nilai = int.Parse(Console.ReadLine());
        Console.Write("Grade Nilainya adalah ");
        string grade = nilai >= 90 && nilai <= 100 ? "A" :
                       nilai >= 70 && nilai <= 89 ? "B" :
                       nilai >= 50 && nilai <= 69 ? "C" :
                       nilai >= 0 && nilai < 50 ? "E" :
                       "Tidak Valid";
        Console.WriteLine(grade);

        Penutup();
    }
    static void Nomor2()
    {
        {
            Console.WriteLine("==========================================");
            Console.WriteLine("== Program Poin Voucher Pulsa ==");
            Console.Write("Masukan Voucher Pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());
            Console.WriteLine("Pulsa " + pulsa);
            string grade = pulsa >= 10000 && pulsa < 25000 ? "80" :
                        pulsa >= 25000 && pulsa < 50000 ? "200" :
                        pulsa >= 50000 && pulsa < 100000 ? "400" :
                        pulsa >= 100000 ? "800" :
                        "Tidak Valid";
            Console.WriteLine($"Point {grade}");
        }

        Penutup();
    }
    static void Nomor3()
    {

        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Diskon Grab ==");
        Console.Write("Masukan Jumlah Belanja: ");
        double belanja = double.Parse(Console.ReadLine());
        Console.Write("Masukan Jarak Antar: ");
        int jarak = int.Parse(Console.ReadLine());
        Console.Write("Masukan Kode Promo: ");
        String kdPromo = Console.ReadLine();
        int minJarak = 5, minOngkir = 5000, ongkir = 0, maxDiskon = 30000;
        double diskon = 0;
        int ongkirNambah = ((jarak - minJarak) * 1000) + minOngkir;
        double diskonBelanja = belanja * 40 / 100;

        Console.WriteLine($"Belanja : {belanja}");
        if (belanja >= 30000 && kdPromo == "JKTOVO")
        {
            diskon = diskonBelanja < 30000 ? diskonBelanja : maxDiskon;
            Console.WriteLine($"Diskon 40%: {diskon}");
        }
        else
        {
            Console.WriteLine($"Diskon 40%: {diskon}");
        }

        if (jarak > minJarak)
        {
            ongkir = ongkirNambah;
            Console.WriteLine($"Ongkos Kirim : {ongkir}");
        }
        else
        {
            ongkir = minOngkir;
            Console.WriteLine($"Ongkos Kirim : {ongkir}");
        }
        double total = belanja - diskon + ongkir;
        Console.WriteLine($"Total Belanja : {total}");

        Penutup();
    }

    static void Nomor4()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Diskon Sopi ==");
        Console.Write("Masukan Jumlah Order: ");
        double order = double.Parse(Console.ReadLine());
        Console.Write("Masukan Ongkos Kirim: ");
        double ongkir = double.Parse(Console.ReadLine());
        Console.Write("Pilih Voucher: ");
        int voucher = int.Parse(Console.ReadLine());
        double diskonOngkir = 0, diskonOrder = 0;
        Console.WriteLine($"Belanja : {order}");
        if (order >= 30000 && voucher == 1)
        {
            diskonOrder = 5000;
            diskonOngkir = 5000;
        }
        else if (order >= 50000 && voucher == 2)
        {
            diskonOrder = 10000;
            diskonOngkir = 10000;
        }
        else if (order >= 100000 && voucher == 3)
        {
            diskonOngkir = 10000;
            diskonOrder = 20000;
        }

        Console.WriteLine($"Ongkos Kirim : {ongkir}");
        Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
        Console.WriteLine($"Diskon Belanja : {diskonOrder}");
        double total = order + ongkir - diskonOrder - diskonOngkir;
        Console.WriteLine($"Total Belanja : {total}");

        Penutup();
    }
    static void Nomor5()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Sebutan Istilah Generasi ==");
        Console.Write("Masukan Nama Anda: ");
        string nama = Console.ReadLine();
        Console.Write("Tahun berapa anda lahir ? ");
        int tahun = int.Parse(Console.ReadLine());
        string istilah = "";
        if (tahun >= 1944 && tahun < 1965)
        {
            istilah = "Baby Boomer";
        }
        else if (tahun >= 1965 && tahun < 1980)
        {
            istilah = "Generasi X";
        }
        else if (tahun >= 1980 && tahun < 1995)
        {
            istilah = "Generasi Y";
        }
        else if (tahun >= 1995 && tahun <= 2015)
        {
            istilah = "Generasi Z";
        }
        else
        {
            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda, tidak masuk dalam istilah");
        }
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {istilah}");

        Penutup();
    }
    static void Nomor6()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Gajian Karyawan ==");
        Console.Write("Masukan Nama : ");
        string nama = Console.ReadLine();
        Console.Write("Masukan Tujangan : ");
        double tunjangan = Convert.ToDouble(Console.ReadLine());
        Console.Write("Masukan Gapok : ");
        double gapok = Convert.ToDouble(Console.ReadLine());
        Console.Write("Masukan Banyaknya Bulan : ");
        int bulan = int.Parse(Console.ReadLine());
        double gapTun = gapok + tunjangan, bpjs = gapTun * 3 / 100;

        Console.WriteLine($"Karyawan dengan nama {nama} slip gaji sebagai berikut : ");
        double pajak = 0;

        if (gapTun > 0 && gapTun <= 5000000)
        {
            pajak = gapTun * 5 / 100;
        }
        else if (gapTun > 5000000 && gapTun <= 10000000)
        {
            pajak = gapTun * 10 / 100;
        }
        else if (gapTun > 10000000)
        {
            pajak = gapTun * 15 / 100;
        }

        Console.WriteLine($"Pajak {pajak}");
        Console.WriteLine($"bpjs {bpjs}");

        double gaji = gapTun - (pajak + bpjs), totalGaji = gaji * 5;
        Console.WriteLine($"gaji/bulan {gaji}");
        Console.WriteLine($"Total gaji/bayak bulan {totalGaji}");

        Penutup();

    }
    static void Nomor7()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Sebutan Body Massa Index ==");
        Console.Write("Masukan Berat Badan Anda (kg): ");
        double berat = Convert.ToDouble(Console.ReadLine());
        Console.Write("Masukan Tinggi Badan Anda (cm): ");
        double tinggi = Convert.ToDouble(Console.ReadLine());
        double tinggiMeter = tinggi / 100;
        double bmi = berat / (tinggiMeter * tinggiMeter);
        string grade = bmi > 0 && bmi < 18.5 ? "Kurus" :
                       bmi >= 18.5 && bmi < 25 ? "Langsing/Sehat" :
                       "Gemuk";
        Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi, 4)}");
        Console.WriteLine($"Anda Termasuk berbadan {grade}");

        Penutup();
    }

    static void Nomor8()
    {
        Console.WriteLine("==========================================");
        Console.WriteLine("== Program Sebutan Nilai Rata-rata ==");
        Console.Write("Masukan Nilai MTK : ");
        double mtk = Convert.ToDouble(Console.ReadLine());
        Console.Write("Masukan Nilai Fisika : ");
        double fisika = Convert.ToDouble(Console.ReadLine());
        Console.Write("Masukan Nilai Kimia: ");
        double kimia = Convert.ToDouble(Console.ReadLine());
        double rata = (mtk + fisika + kimia) / 3;

        string ok = rata >= 50 && rata < 100 ? "Selamat \nKamu Berhasil \nKamu Hebat" :
                    rata > 0 && rata < 50 ? "Maaf \nKamu Gagal" : "Sepertinya Kamu Salah Input Nilai";
        Console.WriteLine($"Nilai Rata-Rata : {Math.Round(rata, 0)} \n{ok}");

        Penutup();
    }
}