﻿using System;
using System.Globalization;
using System.Linq.Expressions;

//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
Nomor5();
static void Nomor1()
{
    Console.Write("Masukan angka awal: ");
    int awal = int.Parse(Console.ReadLine());
    Math.Abs( awal );
    Console.Write("Masukan banyaknya angka: ");
    int banyak = int.Parse(Console.ReadLine());
    Console.Write("Masukan Kelipatan: ");
    int kelipatan = int.Parse(Console.ReadLine());

    for (int i = 0; i < banyak; i++)
    {
        if (awal <= 0)
        {
            if (i % 2 != 1)
            {
                Console.Write("-" + awal + "\t");

            }
            else
            {
                Console.Write(awal + "\t");
            }
            awal = awal + kelipatan;
        }
        else
        {
            if (i % 2 == 1)
            {
                Console.Write("-" + awal + "\t");

            }
            else
            {
                Console.Write(awal + "\t");
            }
            awal = awal + kelipatan;
        }
    }
}

static void Nomor2()
{
    Console.Write("Masukan Datetime (hh:mm:ssAM/PM) : ");
    string dateString = Console.ReadLine();
    DateTime date = DateTime.ParseExact(dateString, "h:mm:sstt", null);
    Console.WriteLine(date.ToString("HH:mm:ss"));

}
static void Nomor3()
{
    Console.Write("Masukan Kode Baju: ");
    int kode = int.Parse(Console.ReadLine());
    string merk="";
    double harga=0,s=0,m=0;
    if (kode > 0 && kode <= 3)
    {
        Console.Write("Masukan Kode Ukuran: ");
        string size = Console.ReadLine().ToLower();
        if (kode == 1)
        {
            s = 200000; m = 220000; harga = 250000000.259;
            merk = "IMP";
        }
        else if (kode == 2)
        {
            s = 150000; m = 160000; harga = 170000;
            merk = "Prada";
        }
        else if (kode == 3)
        {
            s = 200000; m = 200000; harga = 200000;
            merk = "Gucci";
        }

        if(size == "s")
        {
            harga = s;
        }
        if(size == "m")
        {
            harga = m;
        }

        Console.WriteLine($"Merk Baju : {merk}");
        Console.WriteLine($"Harga : {harga.ToString("#,#.00")}");
    }
    else
    {
        Console.WriteLine("Kode Dimasukan Tidak Valid!!!");
    }
}

static void Nomor4()
{
    Console.Write("Masukan Uang Andi : ");
    int uang = int.Parse(Console.ReadLine());
    Console.Write("Masukan Harga Baju : ");
    string kalBaju = Console.ReadLine();
    Console.Write("Masukan Harga Celana : ");
    string kalCelana = Console.ReadLine();
    int iaBaju = 0, iaBacel = 0, iaCel = 0;
    string[] arrayBaju = kalBaju.Split(",");
    string[] arrayCelana = kalCelana.Split(",");
    List<int> list = new List<int>();
    for (int i = 0; i < arrayBaju.Length; i++)
    {
        iaBaju = int.Parse(arrayBaju[i]);
        for (int h = 0; h < arrayCelana.Length; h++)
        {
            iaCel = int.Parse(arrayCelana[h]);
            iaBacel = iaBaju + iaCel;
            if (iaBacel <= uang)
            {
                list.Add(iaBacel);
                for (int k = 0; k < list.Count; k++)
                {
                    for (int j = k + 1; j < list.Count; j++)
                    {
                        if (list[k] > list[j])
                        {
                            int pindah = list[k];
                            list[k] = list[j];
                            list[j] = pindah;
                        }

                    }
                }
            }
        }
    }
    Console.WriteLine(list[list.Count - 1]);
    /*for (int i = 0; i < arrayBaju.Length; i++)
    {
        iaBaju = int.Parse(arrayBaju[i]);
        iaCel = int.Parse(arrayCelana[i]);
        iaBacel = iaBaju + iaCel;

        if (iaBacel <= uang)
        {
            list.Add(iaBacel);
            for (int k = 0; k < list.Count; k++)
            {
                for (int j = k + 1; j < list.Count; j++)
                {
                    if (list[k] > list[j])
                    {
                        int pindah = list[k];
                        list[k] = list[j];
                        list[j] = pindah;
                    }

                }
            }
        }
    }
    Console.WriteLine(list[list.Count - 1]);*/

}

static void Nomor5()
{
    Console.Write("Masukan angka array : ");
    string []angka = Console.ReadLine().Split(",");
    Console.Write("Masukan rot : ");
    int rot = int.Parse(Console.ReadLine());
    for (int i = 0; i<rot; i++)
    {
        Console.Write(i+1+" : ");
        for (int j = i+1; j < angka.Length; j++)
        {   
            Console.Write(angka[j] + ",");
        }
        for (int k = 0; k <= i; k++)
        {
            Console.Write(angka[k]);
            if (k <= i - 1)
            {
                Console.Write(",");
            }
        }
        Console.WriteLine();
    }
}

static void Nomor6()
{
    Console.Write("Masukan angka array : ");
    string[] angkaStr = Console.ReadLine().Split(",");
    int[] angka = new int[angkaStr.Length];

    for (int i = 0; i < angkaStr.Length; i++)
    {
        angka[i] = int.Parse(angkaStr[i]);
    }

    for (int i = 0; i < angka.Length - 1; i++)
    {
        for (int j = i + 1; j < angka.Length; j++)
        {
            if (angka[i] > angka[j])
            {
                int pindah = angka[i];
                angka[i] = angka[j];
                angka[j] = pindah;
            }
        }
    }

    Console.Write("Hasil angka setelah diurutkan: ");
    for (int i = 0; i < angka.Length; i++)
    {
        Console.Write(angka[i]);
        if (i < angka.Length - 1)
        {
            Console.Write(",");
        }
    }
}


static void Nomor7()
{
    Console.Write("Masukan angka awal: ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan Pertambaha/Kelipatan angka: ");
    int tambah = int.Parse(Console.ReadLine());
    Console.Write("Masukan banyaknya angka: ");
    int banyak = int.Parse(Console.ReadLine());
    Console.Write("Masukan karakter +,/,-,*: ");
    char car = char.Parse(Console.ReadLine());
    for (int i = 0; i < banyak; i++)
    {
        Console.Write(awal);
        if (i < banyak - 1)
        {
            Console.Write(",");
        }
        if (car == '+')
        {
            awal = awal + tambah;
        }
        else if (car == '-')
        {
            awal = awal - tambah;
        }
        else if (car == '/')
        {
            awal = awal / tambah;
        }
        else if (car == '*')
        {
            awal = awal * tambah;
        }
    }
}
/*static void Nomor8()
{
    Console.Write("Masukan angka awal: ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan kelipatan angka: ");
    int kelipatan = int.Parse(Console.ReadLine());
    Console.Write("Masukan banyaknya angka: ");
    int banyak = int.Parse(Console.ReadLine());
    for (int i = 0; i < banyak; i++)
    {
        Console.Write(awal);
        if (i< banyak-1)
        {
            Console.Write(",");
        }
        awal = awal * kelipatan;
    }
}*/
static void Nomor9()
{
    Console.Write("Masukan angka awal: ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan Pertambahan angka: ");
    int tambah = int.Parse(Console.ReadLine());
    Console.Write("Masukan banyaknya angka: ");
    int banyak = int.Parse(Console.ReadLine());
    for (int i = 0; i < banyak; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("\t*\t");
        }
        else
        {
            Console.Write(awal);
            awal = awal + tambah;
        }

    }
}