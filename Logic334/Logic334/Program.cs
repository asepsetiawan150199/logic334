﻿// See https://aka.ms/new-console-template for more information
//Output
Console.WriteLine("Hello, World!");
Console.Write("Hello, World!");
Console.WriteLine();
Console.Write("Hello Guys");
Console.WriteLine();
Console.Write("Masukan Nama :");

//Input
string nama = Console.ReadLine();
string kelas = "Batch 334";

string panjang, kali, lebar;
panjang = Console.ReadLine();
kali = Console.ReadLine(); 
lebar = Console.ReadLine();

//eksplisit
int a, b, c;
a = 20;
int umur = 19;//bisa diubah
bool ok = true;
const double phi= 3.14;//bisa diubah

umur = 12;//contoh bisa diubah

//var bisa masuk ke tipe data mana saja atau merubah (implisit)
var n = 'a'; //merubah jadi karakter
var m = "a"; //merubah jadi string
var s = 10; //merubah jadi integer/number
var t = true; //merubah jadi boolean
kelas += " Okeh";

Console.WriteLine(nama);

//Penggabungan String
Console.WriteLine("Hai,{0} {1} Selamat Datang", nama, kelas);
Console.WriteLine(kelas + " Hai, " + nama + " Selamat Datang di kelas "+ kelas);
Console.WriteLine($"Hai,{nama} {kelas} {a} elamat Datang");

Console.ReadKey();