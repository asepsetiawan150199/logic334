﻿//Padleft();
Rekursif();

Console.ReadKey();

static void Rekursif() //memanggil diri sendiri
{
    Console.WriteLine("== Rekursif Function ==");
    Console.Write("Masukan Input Awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukan Input Akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukan Asc atau Dsc : ");
    string type = Console.ReadLine().ToLower();

    //panggil fungsi rekursif
    Perulangan(start, end, type);
}
static int Perulangan2(int start, int end, string type)
{
    if (start == end)
    {
        Console.WriteLine(type == "asc" ? end : start);
        return 0;
    }
    Console.WriteLine(type == "asc" ? start : end);
    // return Perulangan(start, end-1); //kurang
    return Perulangan(start + 1, end, type); //nambah
}
static int Perulangan(int start, int end, string type)
{
    
    if (type == "asc")
    {
        if (start == end)
        {
            Console.WriteLine(start);
            return 0;
        }
        Console.WriteLine(start);
        // return Perulangan(start, end-1); //kurang
        return Perulangan(start + 1, end, type); //nambah

    }
    else
    {
        if (start == end)
        {
            Console.WriteLine(end);
            return 0;
        }
            Console.WriteLine(end);
        // return Perulangan(start, end-1); //kurang
        return Perulangan(start, end-1, type); //nambah

    }

}

static void Padleft()
{
    Console.WriteLine("== Pad Left ==");
    Console.Write("Masukan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukan char : ");
    char chars = char.Parse(Console.ReadLine());

    //231100001 -> 2 digit tahun + 2 gigit bulan + generate lenght
    DateTime date = DateTime.Now;

    string code = ""; //boleh strig.emty

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars); //padright kanan

    Console.WriteLine($"Hasil dari Padleft : {code}");
}