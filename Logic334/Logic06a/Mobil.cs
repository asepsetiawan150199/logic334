﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06a
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platno;
        //Constructor
        public Mobil(string _platno)
        {
            platno = _platno;
        }
        public Mobil()
        {
            
        }
        public string GetPlatNo()
        {
            return platno;
        }
        public void Utama()
        {
            Console.WriteLine($"Nama = {nama}");
            Console.WriteLine($"Bensin = {bensin}");
            Console.WriteLine($"Kecepatan = {kecepatan}");
            Console.WriteLine($"Posisi = {posisi}");
            Console.WriteLine($"Posisi = {platno}");
        }
        public void Percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }
        public void Maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }
        public void IsiBensin(double bensin)
        {
            this.bensin = bensin;
        }
    }
}
