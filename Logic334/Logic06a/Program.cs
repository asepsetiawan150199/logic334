﻿using Logic06a;

//Abstractclass();
//ObjectClass();
//Constraktor();
/*Encapsultion();
Inheritance();*/
Console.ReadKey();

static void Overiffing()
{
    Console.WriteLine("===Overidding===");
    Mamalia kucing = new Mamalia();
    Paus pus = new Paus();
    Console.WriteLine($"Kucing {kucing.pindah()}");
    Console.WriteLine($"Paus {pus.pindah()}");
}
static void Inheritance()
{
    Console.WriteLine("=Enheriten=");
    TypeMobil tipeMobil = new TypeMobil();

    tipeMobil.Civic();
}
static void Encapsultion()
{
    Console.WriteLine("==Encapsulaion==");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;

    pp.TampilkanData();
}
static void Constraktor()
{
    Console.WriteLine("==Contructor==");
    Mobil mobil = new Mobil("B 4C0 T");
    //string platno = mobil.platno;
    string platno = mobil.GetPlatNo();

    Console.WriteLine($"Mobil dengan nomor polisi : {platno}");
}
static void ObjectClass()
{
    Console.WriteLine("==Object Class==");

    Mobil mobil = new Mobil() { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0};

    mobil.Percepat();
    mobil.Maju();
    mobil.IsiBensin(20);
    mobil.Utama();
}

static void Abstractclass()
{
    Console.WriteLine("==Abstrak Klass==");
    Console.WriteLine("Input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine("Input y : ");
    int y = int.Parse(Console.ReadLine()); 

    TextTurunan Calc = new TextTurunan();
    int jumlah = Calc.jumlah(x, y);
    int kurang = Calc.jumlah(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}