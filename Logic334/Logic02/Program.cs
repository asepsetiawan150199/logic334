﻿//IfStatement();
//IfElseStatement();
//IfElseIfStatement();
//IfNestedStatement();
//Ternary();
SwitchCase();
static void IfStatement()
{
    Console.WriteLine("== If Statement ==");
    Console.Write("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Nilai Y : ");
    int y = int.Parse(Console.ReadLine());

    if(x >= 10)
    {
        Console.WriteLine("X is greater than or equals 10");
    }
    if(y <= 5)
    {
        Console.WriteLine("Y is lesser than or equals 5");
    }
    Console.WriteLine("Press Any key to EXIT");
    Console.ReadLine();
}

static void IfElseStatement()
{
    Console.WriteLine("== If Else Statement");
    Console.Write("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("X is greater than or equals 10");
    }
    else
    {
        Console.WriteLine("X is lesser than or equals 10");
    }
}

static void IfElseIfStatement()
{
    Console.WriteLine("== If Else Statement ==");
    Console.Write("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if( x == 10)
    {
        Console.WriteLine("X value equals to 10");
    }
    else if( x > 10)
    {
        Console.WriteLine("X value greater than 10");
    }
    else
    {
        Console.WriteLine("X value less than 10");
    }
}

static void IfNestedStatement()
{
    Console.WriteLine("== If Nested Statement ==");
    Console.Write("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if(x >= 50)
    {
        Console.WriteLine("Kamu Berhasil");
        if(x == 100)
        {
            Console.WriteLine("Kamu Keren");
        }
    }
    else
    {
        Console.WriteLine("Kamu Gagal");
    }
}

static void Ternary()
{
    Console.WriteLine("== Ternary ==");
    Console.Write("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan Nilai Y : ");
    int y = int.Parse(Console.ReadLine());
     //If biasa
    if(x > y)
    {
        Console.WriteLine("X lebih besar dari Y");
    }
    else if(x < y)
    {
        Console.WriteLine("X lebih kecil  dari Y");
    }
    else
    {
        Console.WriteLine("X sama dengan Y");
    }
    //Ternary
    string z = x > y ? "X lebih besar dari y" : x < y ? "X lebih kecil dari y" : "X sama dnegan Y";
    Console.WriteLine(z);

}

static void SwitchCase()
{
    Console.WriteLine("== Swicth Case ==");
    Console.WriteLine("APEL");
    Console.WriteLine("MANGGA");
    Console.WriteLine("JERUK");
    Console.Write("Pilih buah kesukaan kamu : ");
    string pilihan = Console.ReadLine().ToUpper();

    switch (pilihan)
    {
        case "APEL": Console.WriteLine("Anda Memilih buah Apel");
            break; 
        case "JERUK": Console.WriteLine("Anda Memilih buah Jeruk");
            break; 
        case "MANGGA": Console.WriteLine("Anda Memilih buah Mangga");
            break;
        default: Console.WriteLine("Anda Memilih buah Apel");
            break;
    }
}