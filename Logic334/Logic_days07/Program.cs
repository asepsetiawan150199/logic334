﻿//Nomor3 dari HackerRank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'compareTriplets' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY a
     *  2. INTEGER_ARRAY b
     *//*

    public static List<int> compareTriplets(List<int> a, List<int> b)
    {
        int t = 0;
        int u = 0;
        List<int> list = new List<int>();
        for (int i = 0; i < a.Count; i++)
        {
            if (a[i] > b[i])
            {
                t++;
            }
            else if (a[i] < b[i])
            {
                u++;
            }

        }
        list.Add(t);
        list.Add(u);
        return list;

    }

}

class Solution
{
    public static void Main(string[] args)
    {
        

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        List<int> b = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(bTemp => Convert.ToInt32(bTemp)).ToList();

        List<int> result = Result.compareTriplets(a, b);

        Console.WriteLine(String.Join(" ", result));
    }
}*/

//Nomor 4 di HackerRank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
class Result
{

    *//*
     * Complete the 'aVeryBigSum' function below.
     *
     * The function is expected to return a LONG_INTEGER.
     * The function accepts LONG_INTEGER_ARRAY ar as parameter.
     *//*

    public static long aVeryBigSum(List<long> ar)
    {
        long t = 0;
        for (int i = 0; i < ar.Count; i++)
        {
            t = t + ar[i];
        }
        return t;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        

        int arCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<long> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt64(arTemp)).ToList();

        long result = Result.aVeryBigSum(ar);

        Console.WriteLine(result);
    }
}*/

//Nomor 2 HackerRank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'simpleArraySum' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY ar as parameter.
     *//*

    public static int simpleArraySum(List<int> ar)
    {
        int sum = 0;
        for (int i = 0; i < ar.Count; i++)
        {
            sum += ar[i];
        }
        return sum;
    }

}

class Solution
{
    public static void Main(string[] args)
    {

        int arCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt32(arTemp)).ToList();

        int result = Result.simpleArraySum(ar);

        Console.WriteLine(result);

    }
}*/

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

//Nomor 5 Hacker Rank
class Result
{

    *//*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     *//*

    public static void plusMinus(List<int> arr)
    {
        *//*List<decimal> arr1 = new List<decimal>();*//*
        double plus = 0;
        double minus = 0; 
        double zero = 0;
        for (int i = 0; i < arr.Count; i++)
        {
            if (arr[i] > 0)
            {
                plus++;
            }
            else if (arr[i] < 0)
            {
                minus++;
            }
            else if (arr[i]== 0)
            {
                zero++;
            }
        }
        if (plus > 0)
        {
            plus = plus / arr.Count;
        }
        if(minus > 0)
        {

            minus = minus / arr.Count;
        }
        if( zero > 0 )
                { 
            zero = zero / arr.Count;
        }
        *//*
        arr1.Add( plus );
        arr1.Add( minus );
        arr1.Add( zero );*//*
        Console.WriteLine(Math.Round(plus, 6));
        Console.WriteLine(Math.Round(minus, 6));
        Console.WriteLine(Math.Round(zero, 6));
        
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.plusMinus(arr);
    }
}
*/

//Nomor 6 Hacker Rank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'staircase' function below.
     *
     * The function accepts INTEGER n as parameter.
     *//*

    public static void staircase(int n)
    {
        for(int i = 1; i <= n; i++)
        {
            Console.WriteLine(new string(' ', n-i)+ new string('#', i));
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        Result.staircase(n);
    }
}
*/
//Nomor 7 HackerRank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    
     /*
        *    Complete the 'miniMaxSum' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.*//*


/*
    public static void miniMaxSum(List<int> arr)
    {
        int x = arr.Max();
        int y = arr.Min();
        long i = 0;
        long j = 0;
        for(int k = 0; k < arr.Count; k++)
        {
            i += arr[k];
            j += arr[k];
        }
        i -= x; j-=y;

        Console.WriteLine($"{i} {j}");
    }

}

class Solution
{
    public static void Main(string[] args)
    {

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.miniMaxSum(arr);
    }
}*/


//Nomor 8 Hacker Rank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'birthdayCakeCandles' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY candles as parameter.
     *//*

    public static int birthdayCakeCandles(List<int> candles)
    {
        int max = candles.Max();
        int jumlah = 0;
        for (int i = 0; i < candles.Count; i++)
        {
            if (max == candles[i])
            {
                jumlah += 1;
            }
        }
        return jumlah;
    }

}

class Solution
{
    public static void Main(string[] args)
    {

        int candlesCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> candles = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(candlesTemp => Convert.ToInt32(candlesTemp)).ToList();

        int result = Result.birthdayCakeCandles(candles);

        Console.WriteLine(result);

    }
}
*/

// Diagonal Hacker Rank

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{*/

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

/*public static int diagonalDifference(List<List<int>> arr)
{

    int dkanan = 0, dkiri = 0;
    for (int i = 0; i < arr.Count; i++)
    {
        dkanan = dkanan + arr[i][i];
        dkiri = dkiri + arr[i][arr.Count-(i+1)];
    }
    int hasil = Math.Abs(dkanan - dkiri);
    return hasil;
}

}

class Solution
{
public static void Main(string[] args)
{

    int n = Convert.ToInt32(Console.ReadLine().Trim());

    List<List<int>> arr = new List<List<int>>();

    for (int i = 0; i < n; i++)
    {
        arr.Add(Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList());
    }

    int result = Result.diagonalDifference(arr);

    Console.WriteLine(result);


}
}
*/

// grading studen Hacker RAnk

/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{*/

/*
 * Complete the 'gradingStudents' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts INTEGER_ARRAY grades as parameter.
 */

/*  public static List<int> gradingStudents(List<int> grades)
  {
      List<int> result = new List<int>();
      for (int i = 0; i < grades.Count; i++)
      {
          if (grades[i] > 35)
          {
              int ubah = 0;
              if (grades[i] % 5 == 4)
              {
                  ubah = grades[i] + 1;
              }
              else if (grades[i] % 5 == 3)
              {
                  ubah = grades[i] + 2;
              }
              else
              {
                  ubah = grades[i];
              }
              result.Add(ubah);
          }
          else 
          { 
              result.Add(grades[i]);
          }
      }
      return result;
  }

}

class Solution
{
  public static void Main(string[] args)
  {

      int gradesCount = Convert.ToInt32(Console.ReadLine().Trim());

      List<int> grades = new List<int>();

      for (int i = 0; i < gradesCount; i++)
      {
          int gradesItem = Convert.ToInt32(Console.ReadLine().Trim());
          grades.Add(gradesItem);
      }

      List<int> result = Result.gradingStudents(grades);

      Console.WriteLine(String.Join("\n", result));
  }
}
*/

//Date Time Hacker Rank
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{*/

/*
 * Complete the 'timeConversion' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */

/*  public static string timeConversion(string s)
  {
      DateTime result = Convert.ToDateTime(s);
      return result.ToString("HH:mm:ss");
  }

}

class Solution
{
  public static void Main(string[] args)
  {

      string s = Console.ReadLine();

      string result = Result.timeConversion(s);

      Console.WriteLine(result);

      }
}*/

//Apple Orange Hacker Rank
/*
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{*/

/*
 * Complete the 'countApplesAndOranges' function below.
 *
 * The function accepts following parameters:
 *  1. INTEGER s
 *  2. INTEGER t
 *  3. INTEGER a
 *  4. INTEGER b
 *  5. INTEGER_ARRAY apples
 *  6. INTEGER_ARRAY oranges
 */

/*    public static void countApplesAndOranges(int s, int t, int a, int b, List<int> apples, List<int> oranges)
    {
        if (s > t)
        {
            s = t; t = s;
        }
        for (int i = 0; i < apples.Count; i++)
        {
            if (apples[i] + a >= s && apples[i] + a <=t );
        }
        for (int j = 0; j < oranges.Count; j++)
        {

        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        string[] firstMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int s = Convert.ToInt32(firstMultipleInput[0]);

        int t = Convert.ToInt32(firstMultipleInput[1]);

        string[] secondMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int a = Convert.ToInt32(secondMultipleInput[0]);

        int b = Convert.ToInt32(secondMultipleInput[1]);

        string[] thirdMultipleInput = Console.ReadLine().TrimEnd().Split(' ');

        int m = Convert.ToInt32(thirdMultipleInput[0]);

        int n = Convert.ToInt32(thirdMultipleInput[1]);

        List<int> apples = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(applesTemp => Convert.ToInt32(applesTemp)).ToList();

        List<int> oranges = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(orangesTemp => Convert.ToInt32(orangesTemp)).ToList();

        Result.countApplesAndOranges(s, t, a, b, apples, oranges);
    }
}*/

//String Super Reduced String

using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'superReducedString' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static string superReducedString(string s)
    {
        
        for (int i = 0; i < s.Length; i++)
        {
            string c = Convert.ToString(char.IsLetter(s[i]));
            {

            if (c.Contains(s))
            {
              
            }
            }
            
        }
        return s;
    }

}

class Solution
{
    public static void Main(string[] args)
    {

        string s = Console.ReadLine();

        string result = Result.superReducedString(s);

        Console.WriteLine(result);

    }
}
