﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


Nomor10();
static void soal4()
{
    Console.WriteLine("Soal No 4");
    Console.Write("Masukkan tanggal mulai (dd/mm/yyy) : ");
    string input = Console.ReadLine().ToUpper();
    string[] splitInput = input.Split('/');
    int[] splitInputInt = Array.ConvertAll(splitInput, int.Parse);
    try
    {
        DateTime dt1 = new DateTime(splitInputInt[2], splitInputInt[1], splitInputInt[0]);
        Console.Write("Masukkanpelaksaan kelas : ");
        int hariFt1 = int.Parse(Console.ReadLine());
        Console.Write("Masukkan tgl Hari Libur : ");
        string[] hariLibur = Console.ReadLine().Split(",");
        int[] hariLiburInt = Array.ConvertAll(hariLibur, int.Parse);

        for (int i = 0; i < hariFt1; i++)
        {
            if ((int)dt1.DayOfWeek == 0 || (int)dt1.DayOfWeek == 6)
            {
                hariFt1 += 1;
            }
            else
            {
                for (int j = 0; j < hariLiburInt.Length; j++)
                {
                    if (hariLiburInt[j] == dt1.Day)
                    {
                        hariFt1 += 1;
                    }
                }
            }
            dt1 = dt1.AddDays(1);
        }
        Console.WriteLine(dt1.ToString("dd/MM/yyyy"));
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }

}
static void Nomor4()
{
    Console.Write("Tanggal Mulai (dd/MM/yyyy): ");
    string startDateStr = Console.ReadLine();
    DateTime startDate = DateTime.ParseExact(startDateStr, "dd/MM/yyyy", null);
    int hari = startDate.Day;
    Console.Write("Masukkan jumlah hari kelas berlangsung: ");
    int totalClassDays = int.Parse(Console.ReadLine());

    Console.Write("Hari Libur (pisahkan dengan koma): ");
    string holidaysStr = Console.ReadLine();
    string[] holidaysArray = holidaysStr.Split(',');
    int[] intholiday = Array.ConvertAll(holidaysArray, int.Parse);


    DateTime libur = startDate;
    int totalHolidays = holidaysArray.Length;



    for (int i = 0; i < totalClassDays; i++)
    {
        startDate = startDate.AddDays(1);
        while (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday)
        {
            startDate = startDate.AddDays(1);
        }
    }
    DateTime free = new DateTime();
   
    
        for (int i = 0; i < totalHolidays; i++)
    {
        free = libur.AddDays(intholiday[i] - hari);
        if (startDate > free)
        {
            while (free.DayOfWeek != DayOfWeek.Saturday || free.DayOfWeek != DayOfWeek.Sunday)
            {
                startDate = startDate.AddDays(1);
            }
        }
    }
        Console.WriteLine("Tanggal akhir setelah hari libur dan 10 hari kelas: " + startDate.ToString("dd/MM/yyyy"));
}
static void Nomor7()
{
    Console.Write("Total Menu : ");
    int menu = int.Parse(Console.ReadLine());
    Console.Write("Index Makanan Alergi : ");
    int alergi = int.Parse(Console.ReadLine());
    Console.Write("Harga Menu : ");
    string[] hargaMenu = Console.ReadLine().Split(",");
    int[] ihargaMenu = Array.ConvertAll(hargaMenu, int.Parse);
    Console.Write("Uang Elsa : ");
    int uangElsa = int.Parse(Console.ReadLine());
    int makanAlergi = 20000 * alergi;
    int totalMenu = 0;
    for (int i=0; i < ihargaMenu.Length; i++)
    {
        if (ihargaMenu[i] != makanAlergi)
        {
            totalMenu = totalMenu + ihargaMenu[i];
        }
    }
    Console.WriteLine("Elsa Harus Membayar = " + totalMenu/2);
    uangElsa = uangElsa -(totalMenu/2);
    Console.WriteLine($"Sisa Uang Elsa =  {uangElsa}");

}


static void Nomor1()
{
    Console.Write("Masukan nilai X : ");
    int x = int.Parse(Console.ReadLine());
    int hasil = 1;
    string detail = "";
    for (int i = x; i > 0; i--)
    {
        hasil =  i * hasil;
        detail += detail == "" ? i : "x" + i;
    }
    Console.WriteLine($"{x}! = {detail} = {hasil}");
    Console.WriteLine($"ada {hasil} cara");
}

static void Nomor2()
{
    Console.Write("Masukan sandi : ");
    string sandi = Console.ReadLine();

    Console.Write("Masukan kode sandi yang dicari : ");
    string sos = Console.ReadLine();
    int totalsos = 0;
    string kata="";
    string katasalah = "", katabenar = "", katatepat = "";
    for (int i = 0; i < sandi.Length-2; i++)
    {        
        kata = $"{sandi[i]}{sandi[i+=1]}{ sandi[i+=1]}";
        if (kata == sos)
        {
            totalsos++;
            katatepat += kata == "" ? kata : kata;
        }
        else if(kata != sos)
        {
            katasalah += kata == "" ? kata : kata;
            katabenar += kata == "" ? sos : sos;
        }
    }
    int salah = (sandi.Length)-totalsos*3;
    Console.WriteLine("Total sinyal yang salah : "+salah/3);
    Console.WriteLine("Total sinyal yang Tepar : "+totalsos);
    Console.WriteLine("Sinyal Yang Tepat : "+katatepat);
    Console.WriteLine("Sinyal Yang salah : "+katasalah);
    Console.WriteLine("Sinyal Yang benar : "+katabenar);
}
static void Nomor3()
{

        Console.WriteLine($"--Nomor3--");
        Console.Write("Masukkan tanggal pertama (format: dd-MM-yyyy):");
        string date1 = Console.ReadLine();

        Console.Write("Masukkan tanggal kembalinya (format: dd-MM-yyyy):");
        string date2 = Console.ReadLine();

        Console.Write("Masukkan jumlah hari wajib kembali:");
        int dateKembali = int.Parse(Console.ReadLine());

        try
        {
            DateTime datetime1 = DateTime.ParseExact(date1, "d-MM-yyyy", null);
            DateTime datetime2 = DateTime.ParseExact(date2, "d-MM-yyyy", null);
            Console.WriteLine(date1);
            Console.WriteLine(date2);

            TimeSpan interval = datetime2 - datetime1;
            Console.WriteLine($"total hari :{interval.Days} hari");

            int pinalti = interval.Days - dateKembali;
            Console.WriteLine($"Total keterlambatan pengembalian adalah = {pinalti} hari");
            int denda = pinalti * 500;
            Console.WriteLine($"Denda yang harus dibayarkan adalah = {denda} Rupiah");
        }
        catch (Exception ex)
        {
            Console.WriteLine("format yang anda masukkan salah");
            Console.WriteLine("Pesan error : " + ex.Message);
        }

}

static void Nomor5()
{
    Console.Write("Masukan Kalimat : ");
    string kalimat = Console.ReadLine().Replace(" ", ""); //trim untuk menghilangkan posisi spasi dikiri atau dikanan
    int jumlahVokal=0, jumlahKonsonan = 0;
    string vokal = "aiueo";

    for (int i = 0; i < kalimat.Length; i++)
    {
        if (vokal.Contains(kalimat[i]))
        {
            jumlahVokal += 1;
        }
        else
        {
            jumlahKonsonan += 1;
        }
    }
    Console.WriteLine("Jumlah huruf vokal = " + jumlahVokal);
    Console.WriteLine("Jumlah huruf konsonan = " + jumlahKonsonan);
}
static void Nomor6()
{
    Console.Write("Masukan Kata : ");
    string kata = Console.ReadLine();

    for(int i = 0; i < kata.Length; i++)
    {
        Console.WriteLine($"***{kata[i]}***");
    }
}         
static void Nomor8()
{
    Console.Write("Masukan Angka : ");
    int angka = int.Parse(Console.ReadLine());
    for(int i = angka; i > 0; i--)
    {
        for(int j = 1; j <=angka; j++)
        {
            if( j >= i)
            {
                Console.Write("#");
            }
            else
            {
                Console.Write(" ");
            }
        }
        Console.WriteLine();
    }
}
static void Nomor9()
{
    Console.WriteLine("Masukan Angka : ");
    string[] angka = Console.ReadLine().Split(" ");
    int k = 1-1;
    Console.WriteLine("Masukan perkalian matrix : ");
    int x = int.Parse(Console.ReadLine());
    for (int i = 0; i < x; i++)
    {
        for(int j = 0;  j < x; j++)
        {
            Console.Write(angka[k]);
            if( j < 3)
            {
                Console.Write("\t");
            }
            k = k + 1;
        }
        Console.WriteLine();
    }
    int y = x;
    int[] iangka = Array.ConvertAll(angka, int.Parse);
    int dkanan = iangka[0] + iangka[y = y + 1] + iangka[y = y +1];
    y = x;
    int dkiri = iangka[y-1] + iangka[y = y + (y-1)] + iangka[y = y+(y-1)];
    int hasil = dkanan - dkiri;
    Console.WriteLine("Perbedaan diagonal = " + hasil);
}


static void Nomor10()
{
    Console.WriteLine("Masukan tinggi lilin : ");
    int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
    int max = angka.Max();
    int jumlah = 0;
    for (int i = 0; i < angka.Length; i++) 
    { 
        if (max == angka[i])
        {
            jumlah += 1;
        }
    }
    Console.WriteLine(jumlah);
}