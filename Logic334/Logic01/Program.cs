﻿//Konversi();
//OperatorAritmatika();
//Modulus();
//OperatorPenugasan();
//OperatorPerbandingan();
//OperatorLogika();
MethodReturnType();
Console.ReadKey();

static int hasil(int mangga, int apel)
{
    int hasil = 0; //tanpa ini boleh

    hasil = mangga + apel; //hanya ini saja boleh

    return hasil; //tanpa ini boleh
}

static void MethodReturnType()
{
    Console.WriteLine("== Cetak Method Return Typer");
    Console.Write("Masukan Mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan Mangga : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);
    Console.WriteLine($"Hasil Mangga + Apel : {jumlah}");
}
static void OperatorAritmatika(){
    int mangga, apel, hasil = 0;
    Console.WriteLine("== Operator Aritmatika ==");
    //mangga = Convert.ToInt32(Console.ReadLine());
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan Apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;
    Console.WriteLine($"Hasil Mangga + Apel = {hasil}");

}

static void Modulus()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("== Operator Modulus ==");
    //mangga = Convert.ToInt32(Console.ReadLine());
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan Apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga % apel;
    Console.WriteLine($"Hasil Mangga + Apel = {hasil}");

}

static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 8;
    Console.WriteLine("== Operator Penugasan ==");
    //iSI ULANG VARIABEL
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"Mangga : {mangga}");
    Console.Write("Masukan Apel : ");
    apel = int.Parse(Console.ReadLine());

    //Operator Penugasan
    apel += 6;

    Console.WriteLine($"Apel + 6 = {apel}");

}

static void OperatorPerbandingan()
{
    int mangga, apel;
    Console.WriteLine("== Operator Perbandingan ==");
    Console.Write("Masukan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan Apel : ");
    apel = int.Parse(Console.ReadLine());
    
    Console.WriteLine("Hasil Perbandinga");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
}

static void OperatorLogika()
{
    Console.WriteLine("== Operator Logika ==");
    Console.Write(" Enter your age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write(" Password : ");
    string password = Console.ReadLine();

    bool isAdult = age > 18;
    bool isPassword = password == "Admin";

    if (isAdult && isPassword)
    {
        Console.WriteLine("Welcome To The Jungle");
    }
    else
    {
        Console.WriteLine("Sorry, Try Again");
    }

}
//fungsi atau method
static void Konversi()
{
    Console.WriteLine("=== KONVERSI ===");
    int myInt = 10;
    bool myBool = false;
    double myDouble = Convert.ToDouble(Console.ReadLine);
    int myIn = Convert.ToInt32(Console.ReadLine());


    string my = myInt.ToString();
    string my1 = Convert.ToString(myInt);
    double my2 = Convert.ToDouble(myInt);

    Console.WriteLine(my);
    Console.WriteLine(my1);
    Console.WriteLine(my2);
    Console.WriteLine(myBool.ToString());
}