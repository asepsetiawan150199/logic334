﻿
using System;
using System.Reflection;
//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
Nomor6();
//Nomor7();
//Nomor8();

Console.ReadKey();
static void Nomor1()
{
    Console.WriteLine("========Nomor 1========");
    Console.Write("Golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Jam Kerja : ");
    int jamKerja = int.Parse(Console.ReadLine());
    int upah = 0, upahJam = 0, lembur = 0;
        if (golongan == 1)
        {
            upahJam = 2000;
            
        }
        else if (golongan == 2)
        {
            upahJam = 3000;
        
        }
        else if (golongan == 3)
        {
            upahJam = 4000;
       
        
        }
        else if (golongan == 4)
        {
            upahJam = 5000;
        
        }
        else
        {
            Console.WriteLine("Golongan Tidak Valid"); }
    if (upahJam > 0) { 
        if (jamKerja > 40)
        {

            upah = 40 * upahJam;

            lembur = (jamKerja - 40) * (upahJam * 3 / 2);
        }
        else
        {
            upah = jamKerja * upahJam;
        }
        Console.WriteLine($"Upah : {upah}");
        Console.WriteLine($"Lembur : {lembur}");
        Console.WriteLine($"Total : {upah + lembur}");
    }
}

static void Nomor2()
{
    Console.Write("Kalimat : ");
    string kalimat = Console.ReadLine();
    int i = 0;
    string[] katakata = kalimat.Split(" ");
    foreach (string kata in katakata)
    {
        if (kata == "")
        {
            continue;
        }
            Console.WriteLine($"Kata {i + 1}: {kata}");
            i++;
    }
    Console.WriteLine("Total Kata adalah "+i);
}

static void Nomor3()
{
    Console.Write("Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    foreach (string kata in katakata)
    {
        Console.Write(kata[0]);
        for(int i = 0; i< kata.Length-2; i++)
        {
            Console.Write("*");
        }
        Console.Write(kata[kata.Length-1]);
        Console.Write(" ");
        
    }

}
static void Nomor4()
{
    Console.Write("Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    foreach (string kata in katakata)
    {
        string awal = kata.Replace($"{kata[0]}", "*");
        Console.Write(awal[0]);
        for (int i = 1; i < kata.Length - 1; i++)
        {
            Console.Write(kata[i]);
        }
        string akhir = kata.Replace($"{kata[kata.Length - 1]}", "*");
        Console.Write(akhir[akhir.Length-1]+" ");
    }

}
static void Nomor5()
{
    Console.Write("Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    foreach (string kata in katakata)
    {
        Console.Write(kata.Remove(0, 1)+" ");

    }

}

static void Nomor6()
{
    Console.Write("Masukan angka awal: ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukan banyaknya angka: ");
    int banyak = int.Parse(Console.ReadLine());
    Console.Write("Masukan Kelipatan: ");
    int kelipatan = int.Parse(Console.ReadLine());
    Console.Write("Masukan Pemisah : ");
    string pisah = Console.ReadLine();
    for (int i = 0; i < banyak; i++)
    {
        if ( i % 2 == 1)
        {
            Console.Write("\t"+pisah+"\t");
        }
        else
        {
            Console.Write(awal);
            awal = awal * kelipatan;
        }
        
    }
}
static void Nomor7()
{
    Console.Write("Masukan angka pertama: ");
    int kesatu = int.Parse(Console.ReadLine());
    Console.Write("Masukan angka kedua: ");
    int kedua = int.Parse(Console.ReadLine());
    Console.Write("Masukan banyaknya angka selanjutnya: ");
    int banyak = int.Parse(Console.ReadLine());
    int lanjut;
    Console.Write(kesatu + "," + kedua+",");
    for (int i = 0; i < banyak; i++)
    {
        lanjut = kesatu + kedua;
        Console.Write(lanjut);
        if (i<banyak-1)
        {
            Console.Write(",");
        }
        
        kesatu = kedua;
        kedua = lanjut;
    }
}
static void Nomor8()
{
    Console.Write("Masukan n = ");
    int n = int.Parse(Console.ReadLine());
    for (int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            if (i == 0)
            {
                Console.Write(j+1+"\t");
            }
            else if (i == n - 1)
            {
                Console.Write(n-j+"\t");
            }
            else if(j == 0 || j == n- 1) 
                {
                    Console.Write("*\t");
                }
                else
                {
                    Console.Write(" \t");
                }
            }
         Console.WriteLine();
        }
    }
