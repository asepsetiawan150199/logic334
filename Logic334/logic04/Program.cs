﻿//InisialisasiArray();
//MengakseselemenArray();

using Logic04;
//PanggilClassStudent();
//Inisialisist();
//Array2Dimensi();
//MengaksesElemenList();
//InsertList();
//IndexElementList();
//InisialisDateTime();
//ParsingDateTime();
//DateTimeProperties();
TimeSpan();
Console.ReadKey();

static void TimeSpan()
{
    Console.WriteLine("== Time Span ==");
    DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 35);

    //perhitungan interval dari dua tanggal
    TimeSpan interval = date2 - date1;
    Console.WriteLine("Nomor of Days : " + interval.Days);
    Console.WriteLine("Total nomor of days : " + interval.TotalDays);
    Console.WriteLine("Nomor of Hours: " + interval.Hours);
    Console.WriteLine("Total nomor of Hours: " + interval.TotalHours);
    Console.WriteLine("Nomor of Minutes : " + interval.Minutes);
    Console.WriteLine("Total nomor of Minutes: " + interval.TotalMinutes);
    Console.WriteLine("Nomor of Second : " + interval.Seconds);
    Console.WriteLine("Total nomor of Second: " + interval.TotalSeconds);
    Console.WriteLine("Nomor of MiliSecond : " + interval.Minutes);
    Console.WriteLine("Total nomor of MiliSecond : " + interval.Minutes);
    Console.WriteLine("Ticks : " + interval.Ticks);
}
static void DateTimeProperties()
{
    Console.WriteLine("== Date Time Propersties ==");
    DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int milisecond = date.Millisecond;
    int weekday = (int)date.DayOfWeek;
    string hariString = date.DayOfWeek.ToString();
    string hariString2 = date.ToString("dddd");

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"Jam : {jam}");
    Console.WriteLine($"Menit : {menit}");
    Console.WriteLine($"Detik : {detik}");
    Console.WriteLine($"Milisecond : {milisecond}");
    Console.WriteLine($"Hari Weekday : {hariString}");
    Console.WriteLine($"Hari Weekday : {hariString2}");
}
static void ParsingDateTime()
{
    Console.WriteLine("== Parsing Date Time ==");
    Console.Write("Masukan Datetime (dd/MM/yyyy) : ");
    //string dateString = "30/06/2023"; // g bisa kalo bulan dulu 
    string dateString = Console.ReadLine();
    try
    {
    DateTime date1 = DateTime.ParseExact(dateString, "d/MM/yyyy", null); //lebih baik d nya satu
    Console.WriteLine(date1);
    }
    catch(Exception ex)
    {
        Console.WriteLine("format yang anda masukan salah !!!");
        Console.WriteLine("Pesan Error : " +ex.Message);
    }
}
static void InisialisDateTime()
{
    Console.WriteLine("== Inisialis Date Time ==");

    DateTime dt1 = new DateTime(); // 01/01/0001 00.00.00.000
    DateTime dtNow = DateTime.Now;
    Console.WriteLine(dtNow.ToString("dd/MM/yyyy")); //tanggal dan waktu Hari ini
                                                     //MM = Bulan , mm = menit,
                                                     //MMM = Nama Bulan ,
                                                     //MMMM = nama bulan full
    DateTime dt2 = new DateTime(2023, 11, 1);
    DateTime dt3 = new DateTime(2023, 11, 1, 10, 40, 25);

}
static void IndexElementList()
{
    Console.WriteLine("== Index Element List");
    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("4");
    list.Add("3");
    list.Add("4");

    Console.Write("Masukan elemen data : ");
    string item = Console.ReadLine();

    int index = list.IndexOf(item);

    if (index != -1)
    {
        Console.WriteLine($"Element {item} is found at index {index}");
    }
    else
    {
        Console.WriteLine($"Element {item} is not found at index");
    }
}
static void InsertList()
{
    Console.WriteLine("== InsertList List ==");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(6);
    list.Add(3);
    list.Add(4);

    list.Insert(2, 5);
    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}
static void MengaksesElemenList()
{
    Console.WriteLine("== Mengakses Elemen List Dengan Class ==");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);
    list.Add(4);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);

    foreach (int item in list)
    {
        Console.WriteLine(item);
    }

    for (int i= 0;  i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}
static void PanggilClassStudent()
{
    Console.WriteLine("== Panggil Class Student ==");

    //panggil class student/inisialisasi class
    //Student student = new Student();

    //masukan data yang terhubung dengan class student
    List<Student> students = new List<Student>()//statis
    {
        new Student() { Id =  1, Name = "Boboiboy" },
        new Student() { Id =  2, Name = "BoboiboyApi" },
        new Student() { Id =  3, Name = "BoboiboyHalilintar" }
    };
    //tambah data dinamis
    Student student = new Student();
    student.Id = 4;
    student.Name = "BoboiboyAmarah";
    students.Add(student);

    students.Add(new Student() { Id=5, Name= "BoboiboyTopan" });
    Console.WriteLine($"Panjang data list Student = {students.Count}");

    foreach (Student item in students)
    {
        Console.WriteLine($"Id : {item.Id}, Name : {item.Name}");
    }

    Console.WriteLine();

    for(int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id : {students[i].Id}, Name : {students[i].Name}");
    }

   
}
static void InisialisasiArray()
{
    Console.WriteLine("====================");
    Console.WriteLine("== Inisialisasi Array ==");

    //Cara 1
    int[] array = new int[5];

    //Cara 2
    int[] array2 = new int[5] { 1, 2, 3, 4, 5 };

    //Cara 3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };

    //Cara 4
    int[] array4 = { 1, 2, 3, 4, 5 };

    //Cara 5
    int[] array5;
    array5 = new int [5]{ 1,2,3,4,5};

    Console.WriteLine(string.Join(", ", array));
    Console.WriteLine(string.Join(", ", array2));
    Console.WriteLine(string.Join(", ", array3));
    Console.WriteLine(string.Join(", ", array4));
    Console.WriteLine(string.Join(", ", array5));

}
static void MengakseselemenArray()
{
    Console.WriteLine("====================");
    Console.WriteLine("== Inisialisasi Array ==");
    int[] intArray = new int[3];

    //isi data
    intArray[0] = 1;
    intArray[1] = 2;
    intArray[2] = 3;

    //manggil data tanpa looping
    Console.WriteLine(intArray[0]);
    Console.WriteLine(intArray[1]);
    Console.WriteLine(intArray[2]);

    //manggil data dengan looping
    int[] array = { 1, 2, 3 };
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }

    string[] strings = { "Bruno Venus", "Merkurius MX", "Di Bumi Ada Matahari" };
    foreach(string s in strings)
    {
        Console.WriteLine(s);
    }
}
static void StringToArray()
{
    Console.WriteLine("=========================");
    Console.WriteLine("== Ubah String Menjadi Array ==");
    Console.Write("Masukan Kalimat");
    string kalimat = Console.ReadLine();
    char[] charArr = kalimat.ToCharArray();
    foreach (char c in charArr)
    {
        Console.WriteLine(c);
    }
}


static void Array2Dimensi()
{
    Console.WriteLine("== Array 2 Dimensi ==");

    int[,] array = new int[3, 3]
    {
        { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }
    };
    for (int i = 0;i < array.GetLength(0); i++)
    {
        for(int j = 0;j < array.GetLength(1); j++)
        {
            Console.Write(array[i,j]+" ");
        }
        Console.WriteLine();
    }
}
static void Inisialisist()
{
    Console.WriteLine("== Inisiaiis list ==");

    List<string> list = new List<string>()
    {
        "Adu Doe",
        "Oto Bot",
        "Go Pal"
    };
    //tambah data
    list.Add("Boboiboy");

    //ubah data
    list[3] = "Irong Mang";

    //hapus data
    list.Remove("Irong Mang");//jika tidak ada bakal error
    list.RemoveAt(3);//jika tidak ada bakal error\

    //hapus semua

    Console.WriteLine(string.Join(", ",list));
}