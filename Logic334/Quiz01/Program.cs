﻿//Lingkaran();
//Persegi();
//HasilModulus();
//Tugas4();
//Tugas5();
//Tugas6();

Console.ReadKey();
/*static void Utama()
{
    Console.WriteLine("Lingkaran");
    Console.WriteLine("Persegi");
    Console.WriteLine("Modulus");
    Console.WriteLine("Tugas4");
    Console.WriteLine("Tugas5");
    Console.WriteLine("Tugas6");
    Console.WriteLine("Mau Ngerjain Apa ? Pilih Salah Satu ya : ");

}*/
static void Lingkaran()
{
    Console.WriteLine("==========================================");
    Console.WriteLine("== Mencari Luas dan Keliling Lingkaran ==");
    Console.Write("Masukan Jari-jari Lingkaran : ");
    int r = int.Parse(Console.ReadLine());
    //double pi = 3.14;
    double l = Math.PI * r * r;
    double k = 2 * Math.PI * r;
    Console.WriteLine($"Luas Lingkarannnya adalah :{Math.Round(l,2)}");
    Console.WriteLine($"Keliling Lingkarannnya adalah :{k}");
    Console.WriteLine("==========================================");
    Console.WriteLine();
}

static void Persegi()
{
    Console.WriteLine("==========================================");
    Console.WriteLine("== Mencari Luas dan Keliling Persegi ==");
    Console.Write("Masukan Sisi Persegi : ");
    int s = int.Parse(Console.ReadLine());
    int l = s * s;
    int k = 4 * s;
    Console.WriteLine($"Luas Perseginya adalah :{l}");
    Console.WriteLine($"Keliling Perseginya adalah :{k}");
    Console.WriteLine("==========================================");
    Console.WriteLine();
}

static void HasilModulus()
{
    Console.WriteLine("==========================================");
    Console.WriteLine("MENCARI HASIL MODULUS");
    Console.Write("Masukan Angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukan Pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());
    int hasil = angka % pembagi;
    //bisa pake ternary dll
    if(hasil == 0)
    {
        Console.WriteLine($"Angka {angka} % {pembagi} adalah {hasil}");
    }
    else
    {
        Console.WriteLine($"Angka {angka} % {pembagi} hasilnya bukan 0 (nol) melainkan {hasil}");
    }
    Console.WriteLine("==========================================");
    Console.WriteLine();
}
static void Tugas4()
{
    Console.WriteLine("======================================================");
    Console.WriteLine("== Program Pekerjaan Seorang Pemulung Puntung Rokok ==");
    Console.Write("Masukan Banyaknya Puntung Rokok : ");
    int banyak = int.Parse(Console.ReadLine());
    int satuan = 8;
    int untung = 500;
    int terkumpul = banyak / satuan;
    int penghasilan =untung * terkumpul;
    int sisa = banyak % satuan;
    Console.WriteLine($"Jumlah batang yang dihasilkan dari mengumpulkan {banyak} puntung rokok adalah {terkumpul} dengan sisa {sisa} puntung");
    if (terkumpul > 0)
    {
        Console.WriteLine($"Keuntungan yang dihasilkan dari mengumpulkan {terkumpul} batang adalah Rp-{penghasilan}");
    }
}
static void Tugas5()
{
    Console.WriteLine("==========================================");
    Console.WriteLine("== Program Grade Nilai ==");
    Console.Write("Masukan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    Console.Write("Grade Nilainya adalah ");
    string grade = nilai >= 80 && nilai <= 100 ? "A" : nilai >= 60 && nilai < 80 ? "B" : nilai < 60 ? "C" : "Tidak Valid";
    Console.WriteLine(grade);
}
static void Tugas6()
{
    Console.WriteLine("==========================================");
    Console.WriteLine("=== Check Angka Ganjil atau Genap ===");
    Console.Write("Masukan Angka : ");
    int angka = int.Parse(Console.ReadLine());
    int hasil = angka % 2;
    //bisa pake ternary dll
    //string penentuan = hasil == 0 ? $"Angka {angka} adalah Genap" : $"Angka {angka} adalah Ganjil";
    //Console.WriteLine(penentuan);
    if (hasil != 0)
    {
        Console.WriteLine($"Angka {angka} adalah Ganjil");
    }
    else
    {
        Console.WriteLine($"Angka {angka} adalah Genap");
    }
    Console.WriteLine("==========================================");
    Console.WriteLine();
}
/*
jBjkdbejfjs
    vndsnvc
    schgs. ew eadidhf
    akfkas
cbjcjz
*/