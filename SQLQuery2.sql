-- SQLDay02

-- in
select * from mahasiswa where name = 'tunggul'

-- add column nilai pada tabel mahasiswa
alter table mahasiswa add nilai int

-- update column null
update mahasiswa set nilai = 70 where id = 5
update mahasiswa set nilai = 90 where id = 6
update mahasiswa set nilai = 70 where id = 4
-- avg sum
select avg (nilai) from mahasiswa
select sum (nilai), name from mahasiswa group by name

-- inner join atau join
select * from mahasiswa mhs
inner join biodata bio on mhs.id = bio.mahasiswa_id

-- left join 
select * from mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where bio.id is null

-- right join
select * from mahasiswa mhs
right join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.id is null

-- distinct
select distinct name from mahasiswa

-- group by
select name from mahasiswa group by name

-- substring
select SUBSTRING ('SQL Tutorial', 1, 3)

-- charindex
select CHARINDEX('T','Customer')

-- data length
select DATALENGTH ('W3Schools.com')

-- Case when
select name, nilai, 
case 
	when nilai >= 80 then 'A'
	when nilai >= 60 then 'B'
	else 'C'
end 
as grade
from mahasiswa

--concat
select CONCAT ('SQL', 'is', 'fun!')
select 'SQL' + 'is' + 'fun'
select CONCAT ('Nama : ', name) from mahasiswa

-- operator aritmatika
create table penjualan
(
	id int primary key identity (1,1),
	name varchar (50) not null,
	harga decimal(18,0) not null
)

insert into penjualan (name, harga)
values
	('Indomie', 1500),
	('Close Up', 3500),
	('Pepsodent', 3000),
	('Brush Formula', 2500),
	('Roti Manis', 1000),
	('Gula', 3500),
	('Sarden', 4500),
	('Rokok Sampurna', 11000),
	('Dji Sam SOe', 11000)

select * , harga * 100 as [Harga x 100] from penjualan

-- getDate
select GETDATE()

-- day month year
select DAY ('2023-11-10')
select month ('2023-11-10')
select year (getDate())

-- date add
select DATEADD (year, 3 ,GETDATE()) as DateAdd
select DATEADD (year, 3 ,'2023-11-10') as DateAdd

-- get diff
select DATEDIFF(year, '2024-10-10', '2023-10-09')
select DATEDIFF(year, '2023-10-10', '2024-10-09')
select DATEDIFF(DAY, DateAdd(day, 5, GETDATE()), '2024-2-25')

-- sub query
select * 
from mahasiswa mhs
left join biodata bio on mhs.id = bio. mahasiswa_id
where mhs.nilai = (select max(nilai) from mahasiswa)

create table mahasiswa_new
(
	id bigint primary key identity (1,1),
	name varchar (50) not null,
	address varchar (50) not null,
	email varchar (255) null
)

insert into mahasiswa_new (name, address, email)
select name, address, email from mahasiswa

select * from mahasiswa_new

--view
select * 
from mahasiswa mhs
left  join biodata bio using(mahasiswa_id)

-- primary key
create table coba ( id int not null, nama varchar(50) not null)

alter table coba add constraint pk_id primary key (id, name)

-- foreign key

alter table biodata 
add constraint fk_mahasiswa_id 
foreign key (mahasiswa_id) 
references mahasiswa (id)
-- primary key
alter table coba add constraint pk_idnama (id, nama)
-- drop primary key
alter table coba drop constraint pk_nama
--unique key
alter table coba add constraint unique_nama unique (nama)
-- drop unique key
alter table coba drop constraint unique_nama
-- drop foreign key
alter table biodata drop constraint fk_mahasiswa_id

