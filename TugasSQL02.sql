create database DB_Entertainer

--create table tblartis
--(
--	ID INT IDENTITY PRIMARY KEY,
--    kd_artis AS 'A' + RIGHT('00' + CAST(ID AS VARCHAR(100)), 4) PERSISTED,
--	nm_artis varchar (100) not null,
--	jk varchar (10) not null,
--	bayaran int not null,
--	award varchar (4) not null,
--	negara varchar (20) not null                    
--)
select * from tblartis
--drop table tblartis

alter table tblartis alter column bayaran bigint

insert into tblartis (nm_artis, jk, bayaran, award, negara)
values
	('ROBERT DOWNEY JR' , 'PRIA', '3000000000', '2', 'AS'),
	('ANGELINA JOLIE' , 'WANITA', '7000000000', '1', 'AS'),
	('JACKIE CHAN' , 'PRIA', '2000000000', '7', 'HK'),
	('JOE TASLIM' , 'PRIA', '3500000000', '1', 'ID'),
	('CHELSEA ISLAN' , 'WANITA', '3000000000', '0', 'ID')

create table tblfilm
(
	ID INT IDENTITY PRIMARY KEY,
    kd_film AS 'F' + RIGHT('00' + CAST(ID AS VARCHAR(100)), 4) PERSISTED,
	nm_film varchar (100) not null,
	genre varchar (10) not null,
	artis varchar (10) not null,
	producer varchar (10) not null,
	pendapatan bigint not null,
	nominasi int not null
)

insert into tblfilm (nm_film, genre, artis, producer, pendapatan, nominasi)
values
	('IRON MAN' , 'G001', 'A001', 'PD01', '2000000000', '3'),
	('IRON MAN 2' , 'G001', 'A001', 'PD01', '1800000000', '2'),
	('IRON MAN 3' , 'G001', 'A001', 'PD01', '1200000000', '0'),
	('IRON AVENGER CIVIL WAR' , 'G001', 'A001', 'PD01', '2000000000', '1'),
	('SPIDERMAN HOME COMING' , 'G001', 'A001', 'PD01', '1300000000', '0'),
	('THE RAID' , 'G001', 'A004', 'PD03', '800000000', '5'),
	('FAST & FURIOUS' , 'G001', 'A004', 'PD05', '830000000', '2'),
	('HABIBIE DAN AINUN' , 'G004', 'A005', 'PD03', '670000000', '4'),
	('POLICE STORY' , 'G001', 'A003', 'PD02', '700000000', '3'),
	('POLICE STORY 2' , 'G001', 'A003', 'PD02', '710000000', '1'),
	('POLICE STORY 3' , 'G001', 'A003', 'PD02', '615000000', '0'),
	('RUSH HOUR' , 'G003', 'A003', 'PD05', '695000000', '2'),
	('KUNGFU PANDA' , 'G003', 'A003', 'PD05', '923000000', '5')

--create table tblproduser
--(
--	ID INT IDENTITY PRIMARY KEY,
--    kd_produser AS 'PD' + RIGHT('0' + CAST(ID AS VARCHAR(50)), 4) PERSISTED,
--	nm_produser varchar (50) not null,
--	international varchar (10) not null
--)

insert into tblproduser(nm_produser, international)
values
	('MARVEL' , 'YA'),
	('HONGKONG CINEMA' , 'YA'),
	('RAPI FILM' , 'TIDAK'),
	('PARKIT' , 'TIDAK'),
	('PARAMOUNT PICTURE' , 'YA')

--create table tblnegara
--(
--	kd_negara varchar (10) not null,
--	nm_negara varchar (100) not null
--)

insert into tblnegara (kd_negara, nm_negara)
values
	('AS' , 'AMERIKA SERIKAT'),
	('HK' , 'HONGKONG'),
	('ID' , 'INDONESIA'),
	('IN' , 'INDIA')

--create table tblgenre
--(
--	ID INT IDENTITY PRIMARY KEY,
--    kd_genre AS 'G' + RIGHT('00' + CAST(ID AS VARCHAR(50)), 4) PERSISTED,	
--	nm_genre varchar (50) not null
--)

insert into tblgenre (nm_genre)
values
	('ACTION'),
	('HORROR'),
	('COMEDY'),
	('DRAMA'),
	('THRILLER'),
	('FICTION')

select * from tblproduser 
select * from tblfilm

-- Nomor 1
select P.nm_produser, sum (F.pendapatan) as pendapatan
from tblproduser as P join tblfilm as F
on P.kd_produser = F.producer
where P.nm_produser = 'MARVEL'
group by P.nm_produser

-- Nomor 2
select nm_film, nominasi
from tblfilm
where nominasi = 0

-- Nomor 3
select nm_film 
from tblfilm
--where SUBSTRING (nm_film, 1, 1) ='p'
where right (nm_film, 1) = 'y'

-- Nomor 4
select nm_film 
from tblfilm
--where nm_film like '%Y'
where SUBSTRING (nm_film, DATALENGTH(nm_film), 1) ='y'

-- Nomor 5
select nm_film 
from tblfilm
where nm_film like '%D%'

-- Nomor 6
select nm_film, nm_artis
from tblfilm as F join tblartis as A
on F.artis = A.kd_artis

-- Nomor 7
select nm_film, negara, nm_negara
from tblfilm as F join tblartis as A
on F.artis = A.kd_artis
join tblnegara as N 
on N.kd_negara = A.negara
--where A.negara = 'HK' or A.negara = 'id'
--or A.negara = 'in'
where A.negara in ('hk' ,'id')

-- Nomor 8
select nm_film, nm_negara
from tblfilm as F join tblartis as A
on F.artis = A.kd_artis join tblnegara as N
on A.negara = N.kd_negara
where N.nm_negara not like '%o%'

-- Nomor 9
select nm_artis
from tblartis A left join tblfilm F
on A.kd_artis = F.artis
where F.artis is null

-- Nomor 10
select nm_artis, nm_genre
from tblartis A join tblfilm F
on A.kd_artis = F.artis join tblgenre G
on G.kd_genre = F.genre
where nm_genre = 'DRAMA'

-- Nomor 11
select A.nm_artis, G.nm_genre
from tblgenre G join  tblfilm F
on G.kd_genre = F.genre join tblartis A
on A.kd_artis = F.artis
where G.nm_genre = 'ACTION'
group by A.nm_artis, G.nm_genre -- bisa pakai distinct diawal sesudah select

-- Nomor 12
select kd_negara, nm_negara, count (artis) as jumlah_film
from tblnegara N left join tblartis A
on N.kd_negara = A.negara left join tblfilm F
on F.artis = A.kd_artis
group by kd_negara, nm_negara

select kd_negara, nm_negara, count (artis) as jumlah_film
from tblfilm F join tblartis A
on   A.kd_artis =F.artis   right join tblnegara N
on A.negara=N.kd_negara
group by kd_negara, nm_negara

-- Nomor 13
select F.nm_film, nm_produser
from tblfilm F join tblproduser P
on F.producer = P.kd_produser
where P.international = 'YA' and nm_produser in ('Hongkong cinema', 'marvel')

-- Nomor 14
select P.nm_produser, count (F.nm_film) as jumlah_film
from tblproduser P left join tblfilm F
on P.kd_produser = F.producer
where nm_produser = 'marvel'
group by nm_produser
having COUNT (NM_FILM) >=3