create database DB_Sales

--create table SALESPERSON 

create table SALESPERSON
(
	ID INT IDENTITY PRIMARY KEY,
    NAME varchar (50) not null,
	BOD date not null,
	SALARY decimal (18,2) not null
)
alter table Salesperson alter column salary decimal (18,2) not null
insert into SALESPERSON (NAME, BOD, SALARY)
values
	('Abe' , '9/11/1988', '140000'),
	('Bob' , '9/11/1978', '44000'),
	('Chris' , '9/11/1983', '40000'),
	('Dan' , '9/11/1980', '52000'),
	('Ken' , '9/11/1977', '115000'),
	('joe' , '9/11/1990', '38000')

create table ORDERS
(
	ID INT IDENTITY PRIMARY KEY,    
	ORDER_DATE date not null,
	CUST_ID int not null,
	SALESPERSON_ID int not null,
	AMOUNT decimal (18,2) not null
)
insert into ORDERS (ORDER_DATE, CUST_ID, SALESPERSON_ID, AMOUNT)
values
	('8/2/2020' , '4', '2', '540'),
	('1/22/2021' , '4', '5', '1800'),
	('7/14/2019' , '9', '1', '460'),
	('1/29/2018' , '7', '2', '2400'),
	('2/3/2021' , '6', '4', '600'),
	('3/2/2020' , '6', '4', '720'),
	('5/6/2021' , '9', '4', '150')

-- Nomor 1
select NAME as [Nama Sales], count(O.SALESPERSON_ID) as [Jumlah Order]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
group by NAME
having count(O.SALESPERSON_ID) > 1

-- Nomor 2
select NAME as [Nama Sales], sum(O.AMOUNT) as [Jumlah order diatas 1000]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
group by NAME
having sum(O.AMOUNT) > 1000


-- Nomor 3
select NAME as [Nama Sales],-- datediff(year ,BOD, GETDATE()) as Umur
floor(datediff(DAY ,BOD, GETDATE())/365.25) as Umur, SALARY as Gaji, sum(O.AMOUNT) as [Total Amount]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
where year(ORDER_DATE) >=  '2020'
group by S.NAME, S.SALARY,S.BOD
order by Umur asc


-- Nomor 4
select NAME as [Nama Sales],sum(O.AMOUNT) as [Total Amount],
avg(O.AMOUNT) as [Rata-rata]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
group by S.NAME
order by avg(O.Amount) desc

-- Nomor 5
select SALARY*0.3 as Bonus, NAME as [Nama Sales],
count(O.SALESPERSON_ID) as [Total Order]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
group by SALARY, S.NAME
having count(O.SALESPERSON_ID) > 2

-- Nomor 6
select NAME, count(O.SALESPERSON_ID) as [Total Order]
from SALESPERSON S left join ORDERS O
on S.ID = O.SALESPERSON_ID
group by NAME
having count(O.SALESPERSON_ID) < 1
select NAME, SALESPERSON_ID
from SALESPERSON S left join ORDERS O
on S.ID = O.SALESPERSON_ID
where SALESPERSON_ID is null
-- Nomor 7
select NAME, count(O.SALESPERSON_ID) as [Total Order], SALARY,
SALARY*0.02 as [Gaji Di Potong], SALARY-(SALARY*0.02) as [Gaji Di Terima]
from SALESPERSON S left join ORDERS O
on S.ID = O.SALESPERSON_ID
group by NAME, SALARY
having count(O.SALESPERSON_ID) < 1