create database DBPenerbit

create table tblPengarang
(
	id int primary key identity (1,1),
	kd_Pengarang varchar (7) not null,
	Nama varchar (30) not null,
	Alamat varchar (80) not null,
	Kota varchar (15) not null,
	Kelamin varchar (1) not null
)

create table tblGaji
(
	ID int primary key identity (1,1),
	kd_Pengarang varchar (7) not null,
	Nama varchar (30) not null,
	Gaji decimal (18,4) not null
)

insert into tblPengarang (Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
values
	('P0001' , 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
	('P0002' , 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
	('P0003' , 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
	('P0004' , 'Siti', 'Jl. Durian 15', 'Solo', 'w'),
	('P0005' , 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
	('P0006' , 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
	('P0007' , 'Jaja', 'Jl. singa 7', 'Bandung', 'P'),
	('P0008' , 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
	('P0009' , 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
	('P0010' , 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'w')

insert into tblGaji (Kd_Pengarang, Nama, Gaji)
values
	('P0002' , 'Rian', '600000'),
	('P0005' , 'Amir', '700000'),
	('P0004' , 'Siti', '500000'),
	('P0003' , 'Suwardi', '1000000'),
	('P0010' , 'Fatmawati', '600000'),
	('P0008' , 'Saman', '750000')

-- Nomor 1
 select count (ID) as Jumlah_Pengarang 
 from tblPengarang
-- Nomor 2
select count (ID) as Jumlah_Pengarang, Kelamin
from tblPengarang
group by Kelamin 
order by Kelamin desc
---Nomor 3
select Kota, count (Kota) as Jumlah_Kota
from tblPengarang
group by Kota
-- Nomor 4
select Kota, count (Kota) as Jumlah_Kota
from tblPengarang
group by Kota
having count (Kota) > 1
-- Nomor 5
select max (Kd_Pengarang) as Kd_Pengarang_Terbesar, 
	   min (Kd_Pengarang) as Kd_Pengarang_Terkecil
from tblPengarang
-- Nomor 6
select max (Gaji) as Tertinggi, min (Gaji) as Terendah
from tblGaji
-- Nomor 7
select Gaji
from tblGaji
where Gaji > 600000
order by Gaji asc
-- Nomor 8
select sum (Gaji) as Jumlah_Gaji
from tblGaji
-- Nomor 9
select sum (Gaji) as Jumlah_Gaji, Kota
from tblGaji as G join tblPengarang as P 
on G.kd_Pengarang = P.Kd_Pengarang 
group by P.Kota
-- Nomor 10
select *
from tblPengarang
where Kd_Pengarang between 'P0003' and 'P0006'
-- Nomor 11
select *
from tblPengarang
--where Kota = 'Yogya' or Kota = 'solo' or Kota = 'Magelang'
where Kota in ('Yogya','solo','Magelang')
-- Nomor 12
select *
from tblPengarang
where not Kota = 'Yogya'
-- Nomor 13
-- A
select *
from tblPengarang
where 
-- A
	--Nama like 'A%' 
-- B
	--Nama like '%I' 
-- C
	--Nama like '__A%' 
-- D
	Nama not like '%N'
-- Nomor 14
select *
from tblPengarang as P join tblGaji as G
on P.Kd_Pengarang = G.Kd_Pengarang
-- Nomor 15
select Kota, Gaji
from tblPengarang as P join tblGaji as G
on P.Kd_Pengarang = G.Kd_Pengarang
where G.Gaji < 1000000
-- Nomor 16
alter table tblPengarang alter column Kelamin varchar (10) not null
-- Nomor 17
alter table tblPengarang add Gelar varchar (12) null
-- Nomor 18
update tblPengarang
set 
	Alamat = 'Jl. Cendrawasih 65', 
	Kota = 'Pekanbaru'
where Nama = 'Rian'
-- Nomor 19
create view vwPengarang
as select P.Kd_Pengarang, P.Nama, P.Kota, G.Gaji 
from tblPengarang as P join tblGaji as G
on P.Kd_Pengarang = G.Kd_Pengarang
-- Memanggil view
select * from vwPengarang